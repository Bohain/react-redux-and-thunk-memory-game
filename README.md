# React Redux and Thunk Memory Game

React memory card game using Redux and Thunk. You can select player mode, player's name, and it displays a little leaderboard (solo mode).
Both React and json-server must be running, json-server calls are setted up on port 3004.

## Setup

### React

yarn install

yarn start

### JSON Server

#### JSON Server install
npm i -g json-server

#### JSON Server start
json-server --watch db.json -p 3004