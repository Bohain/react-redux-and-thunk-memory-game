/*
Import
 */
// React
import React from 'react';
import PropTypes from 'prop-types';
// Inner
import LeaderboardItem from './LeaderboardItem';
//

/*
Component
 */
const LeaderboardList = ({ soloLeaderboard, onBackwardClick }) => (
    <div className="wrapper-home">
        <h1>Classement</h1>
        <ul className="leaderboard-list">
            {soloLeaderboard.map((scoreInfos, index) => (
                <LeaderboardItem key={index} {...scoreInfos} />
            ))}
        </ul>
        <button className="button" onClick={() => onBackwardClick(0)}>Retour</button>
    </div>
)
//

/*
PropTypes
 */
LeaderboardList.PropTypes = {
    soloLeaderboard: PropTypes.arrayOf(
        PropTypes.shape({
            playerInfos: PropTypes.shape({
                p1: PropTypes.shape({
                    nom: PropTypes.string.isRequired
                }).isRequired
            }).isRequired,
            timer: PropTypes.number.isRequired
        }).isRequired
    ).isRequired,
    onBackwardClick: PropTypes.func.isRequired
}
//

/*
Export
 */
export default LeaderboardList;
//