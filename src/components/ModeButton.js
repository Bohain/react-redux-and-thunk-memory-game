import React from 'react';
import PropTypes from 'prop-types';

const ModeButton = ({ onClick, label }) => (
    <button
        className="mode-select-button button"
        type="button"
        onClick={onClick}
    >{label}</button>
)

ModeButton.PropTypes = {
    onClick: PropTypes.func.isRequired,
    label: PropTypes.string.isRequired
}

export default ModeButton;