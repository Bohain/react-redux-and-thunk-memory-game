/*
Import
 */
// React
import React from 'react';
import PropTypes from 'prop-types';
//

/*
Functions
 */
const renderInfos = ({ playersInfos, timer, gameMode }) => {
    if(gameMode === 0)
        return (
            <div className="solo-timer">
                Temps : {timer}
            </div>
        );
    else if(gameMode === 1)
        return (
            <div className="duo-timer">
                <span>
                    {playersInfos.p1.name}, score : {playersInfos.p1.score}
                </span>
                <span>
                    <strong>{playersInfos.roundCount % 2 ? playersInfos.p2.name : playersInfos.p1.name}</strong> doit jouer !
                </span>
                <span>
                    { playersInfos.p2.name }, score: { playersInfos.p2.score }
                </span >
            </div>
        );
}
//

/*
Component
 */
const Infos = ({ playersInfos, timer, gameMode }) => (
    renderInfos({ playersInfos, timer, gameMode })
)
//

/*
PropTypes
 */
Infos.PropTypes = {
    gameMode: PropTypes.number.isRequired,
    playersInfos: PropTypes.shape({
        p1: PropTypes.shape({
            name: PropTypes.string.isRequired,
            score: PropTypes.number
        }).isRequired,
        p2: PropTypes.shape({
            name: PropTypes.string.isRequired,
            score: PropTypes.number.isRequired
        }),
        roundCount: PropTypes.number
    }).isRequired,
    timer: PropTypes.number.isRequired
}
//

/*
Export
 */
export default Infos;
//