/*
Import
 */
// React
import React from 'react';
import PropTypes from 'prop-types';
//

/*
Component
 */
const LeaderboardItem = ({playerInfos, timer}) => (
    <li>
        <span className="username">{playerInfos.p1.name}</span><span className="timer">{timer}</span>
    </li>
)
//

/*
PropTypes
 */
LeaderboardItem.PropTypes = {
    playerInfos: PropTypes.shape({
        p1: PropTypes.shape({
            nom: PropTypes.string.isRequired
        }).isRequired
    }).isRequired,
    timer: PropTypes.number.isRequired
}
//

/*
Export
 */
export default LeaderboardItem;
//