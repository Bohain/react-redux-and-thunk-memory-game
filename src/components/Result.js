/*
Import
 */
// React
import React from 'react';
import PropTypes from 'prop-types';
//

/*
Functions
 */
function renderResult({ playersInfos, timer, gameMode }) {
    if(gameMode === 0)
        return(
            <div>
                <h2>Temps final</h2>
                {timer}
            </div>
        )
    else if (gameMode === 1)
        return (
            <div>
                <h2>{playersInfos.winner.name} a gagné la partie</h2>
                avec {playersInfos.winner.score} points !
            </div>
        )
}
//

/*
Component
 */
const Result = ({ playersInfos, timer, gameMode, onHomeClick }) => (
    <div className="result-wrapper">
        {renderResult({ playersInfos, timer, gameMode })}
        <button
            className="button"
            onClick={ onHomeClick }
        >Accueil</button>
    </div>
)
//

/*
PropTypes
 */
Result.PropTypes = {
    gameMode: PropTypes.number.isRequired,
    playersInfos: PropTypes.shape({
        p1: PropTypes.shape({
            name: PropTypes.string.isRequired,
            score: PropTypes.number
        }).isRequired,
        p2: PropTypes.shape({
            name: PropTypes.string.isRequired,
            score: PropTypes.number.isRequired
        }),
        roundCount: PropTypes.number
    }).isRequired,
    timer: PropTypes.number.isRequired,
    onHomeClick: PropTypes.func.isRequired
}
//

/*
Export
 */
export default Result;
//