/*
Imports
 */
// React
import { connect } from 'react-redux'
// Inner
import { selectCard } from '../actions';
import Board from '../components/Board';
//

/*
Functions
 */
function renderTimer(timer) {
    let min = Math.floor(timer / 60);
    let sec = Math.floor(timer - (min * 60));
    return `${min} min ${sec} sec`
}
//

/*
State and Actions dispatch
*/
// State
const mapStateToProps = state => {
    return {
        gameMode: state.gameInfos.mode,
        playersInfos: state.gameInfos.playersInfos,
        timer: renderTimer(state.gameInfos.timer),
        cards: state.gameCards.cards
    }
};
// Actions dispatch
const mapDispatchToProps = dispatch => {
    return {
        onCardClick: (index, value) => {
            dispatch(selectCard(index, value));
        }
    }
}
//

/*
Export
 */
const BoardContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Board)

export default BoardContainer;
//