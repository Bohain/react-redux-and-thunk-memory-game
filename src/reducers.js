/*
Imports
 */
// Redux
import { combineReducers } from 'redux';
// Inner
import {
    END_GAME,
    RESET_GAME,
    INCREMENTS_TIMER,
    RECEIVE_CARDS,
    RECEIVE_LEADERBOARD,
    SET_MODE,
    TIMER_START,
    TOGGLE_CARD,
    UPDATE_CARDS_LIST,
    UPDATE_PLAYERS_INFOS, 
    SET_PLAYER_NAME, 
    SET_GAME_STATE,
    UPDATE_LEADERBOARD
} from './actions';
//

/*
Reducers
 */
function gameInfos(state = {mode: 0, gameState: 0, playersInfos: {p1: {name: ''}}}, action) {
    switch (action.type) {
        case SET_MODE:
            if(action.mode === 1)
                return {
                    ...state,
                    mode: action.mode,
                    playersInfos: {
                        p1: {
                            name: '',
                            score: 0
                        },
                        p2: {
                            name: '',
                            score: 0
                        },
                        roundCount: 0
                    }
                };
            else if(action.mode === 0)
                return {
                    ...state,
                    mode: action.mode,
                    playersInfos: {
                        p1: {
                            name: ''
                        }
                    }
                };
            break;
        case SET_PLAYER_NAME:
            return {
                ...state,
                playersInfos: action.playersInfos
            }
        case SET_GAME_STATE:
            return {
                ...state,
                gameState: action.gameState
            }
        case TIMER_START:
            return {
                ...state,
                timer: 1
            }
        case INCREMENTS_TIMER:
            return {
                ...state,
                timer: state.timer + 1
            }
        case RECEIVE_CARDS:
            return {
                ...state,
                gameState: 1,
                cardsCount: action.cardsCount
            }
        case UPDATE_PLAYERS_INFOS:
            return {
                ...state,
                playersInfos: action.playersInfos
            }
        case END_GAME:
            return {
                ...state,
                gameState: 2,
                playersInfos: action.playersInfos
            }
        case RESET_GAME:
            return {
                mode: 0,
                gameState: 0,
                playersInfos: {
                    p1: {
                        name: ''
                    }
                }
            }
        default:
            return state;
    }
}

function gameCards(state = {}, action) {
    switch (action.type) {
        case RECEIVE_CARDS:
            return {
                ...state,
                selectedCards: [],
                cards: action.cards
            };
        case TOGGLE_CARD:
            return {
                ...state,
                selectedCards: [...state.selectedCards, {index: action.cardIndex, value: action.value}],
                cards: action.cards
            };
        case UPDATE_CARDS_LIST:
            return {
                ...state,
                selectedCards: [],
                cards: action.cards
            }
        case END_GAME:
            return {}
        default:
            return state;
    }
}

function gameLeaderboard(state = {leaderboard: {solo: []}}, action) {
    switch(action.type) {
        case RECEIVE_LEADERBOARD:
            return {
                ...state,
                leaderboard: action.leaderboard
            }
        case UPDATE_LEADERBOARD:
            return {
                ...state,
                leaderboard: action.leaderboard
            }
        default:
            return state;
    }
}
//

/*
Export
 */
const memoryApp = combineReducers({
    gameInfos,
    gameCards,
    gameLeaderboard
})

export default memoryApp;
//