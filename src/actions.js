/*
Imports
 */
import fetch from 'node-fetch';
//

/*
Actions types
 */
// Game infos
export const SET_MODE = 'SET_MODE';
export const SET_PLAYER_NAME = 'SET_PLAYER_NAME';
export const SET_GAME_STATE = 'SET_GAME_STATE';
export const TIMER_START = 'TIMER_START';
export const INCREMENTS_TIMER = 'INCREMENTS_TIMER';
export const UPDATE_PLAYERS_INFOS = 'UPDATE_PLAYERS_INFOS';
export const END_GAME = 'END_GAME';
export const RESET_GAME = 'RESET_GAME';
// Leaderboard
export const UPDATE_LEADERBOARD = 'UPDATE_LEADERBOARD';
// API Fetch
export const RECEIVE_LEADERBOARD = 'RECEIVE_LEADERBOARD';
export const RECEIVE_CARDS = 'RECEIVE_CARDS';
// Cards Actions
export const UPDATE_CARDS_LIST = 'UPDATE_CARDS_LIST';
export const TOGGLE_CARD = 'TOGGLE_CARD';
//

/*
Other constants
 */
let timerAction = null; // Variable qui permet de gérer l'état du timer (start, increments, stop)
//

/*
Action creators
 */

/*
Gestion de l'initialisation du jeu
 */
// Mélange le paquet de cartes
function shuffleCards(deck) {
    for(let i = 0 ; i < deck.length ; i++) {
        let j = i + Math.floor(Math.random() * (deck.length - i));
        let temp = deck[j];
        deck[j] = deck[i];
        deck[i] = temp;
    }
    return deck;
}
// Reçoit l'ensemble des informations, dont les cartes et initialise la partie avec le deck mélangé
function receiveCards(cardsCount, json) {
    let cards = json.cards;
    // Ajout des états pour chaque carte
    cards.map(card => {
        card.isSelected = false;
        card.isFound = false;
        return card;
    });
    // Copie des cartes dans un deuxième tableau
    const cardsClone = JSON.parse(JSON.stringify(cards));
    // Fusion des cartes tout en les mélangeant
    cards = shuffleCards([...cards, ...cardsClone]);
    return {
        type: RECEIVE_CARDS,
        cards,
        cardsCount
    }
}
// Récupération d'un deck de cartes via l'API deckofcardsapi (on divise cardsCount par deux pour dupliquer le deck dans receiveCards)
function fetchCards(cardsCount) {
    return dispatch => {
        return fetch(`https://deckofcardsapi.com/api/deck/new/draw/?count=${cardsCount/2}`)
            .then(response => response.json())
            .then(json => dispatch(receiveCards(cardsCount, json)))
    }
}
//

/*
 Fonctions pour la gestion du timer
 */
// Trigger l'action pour lance le timer
function timerInit() {
    return {
        type: TIMER_START
    }
}
// Lance le timer en initialisant l'action qui incrémente le timer
function startTimer() {
    return dispatch => {
        timerAction = null;
        clearInterval(timerAction);
        timerAction = setInterval(() => dispatch(incrementsTimer()), 1000);
        dispatch(incrementsTimer());
        return dispatch(timerInit());
    }
}
// Trigger l'action qui incrémente le timer
function incrementsTimer() {
    return {type: INCREMENTS_TIMER}
}
// Stop le timer pour la fin du jeu
function stopTimer() {
    clearInterval(timerAction);
}
//

/*
Gestion des actions du jeu
 */
// Vérifie si deux cartes sont sélectionnées
function shouldCompareCards(selectedCards) {
    if(selectedCards.length > 1)
        return true;
    else
        return false;
}
// Compare deux cartes et renvoie true si elles sont égales, false sinon
function compareCards(selectedCards) {
    if(selectedCards[0].value === selectedCards[1].value)
        return true;
    else
        return false;
}
// Reset les cartes sélectionnées à la fin d'un round
function resetSelectedCards(cards) {
    cards.map(card => {
        card.isSelected = false;
        return card;
    })
    return cards;
}
// Mets à jour la liste de cartes trouvées si compareResult = true
function updateCardsList(compareResult, cards, selectedCards) {
    if(compareResult) {
        cards.map((card, index) => {
            if (index === selectedCards[0].index || index === selectedCards[1].index) {
                card.isSelected = false;
                card.isFound = true;
            }
            return card;
        });
    }
    return {type: UPDATE_CARDS_LIST, cards}
}
// Mets à jour les informations des joueurs si le mode === 1
function updatePlayersInfos(compareResult, playersInfos) {
    if(compareResult) {
        if(playersInfos.roundCount%2)
            playersInfos.p2.score++;
        else
            playersInfos.p1.score++;
    }
    playersInfos.roundCount++;
    return {type: UPDATE_PLAYERS_INFOS, playersInfos}
}
// Vérifie si le jeu est fini
function shouldEndGame(cardsCount, cards) {
    let foundCards = 0;
    for(let i = 0 ; i < cards.length ; i++) {
        if(cards[i].isFound)
            foundCards++;
    }
    if(foundCards === cardsCount)
        return true;
    return false;
}
// Effectue la fin d'une partie
function endGame(dispatch, gameInfos) {
    // Stop le timer
    stopTimer();
    // Variable pour définir l'url vers le serveur
    const apiUrl = `http://localhost:3004/`;
    let mode = ``;
    let bodyContent = {};
    // Check le mode de la partie
    if(gameInfos.mode === 1) {
        mode = `multi`;
        if(gameInfos.playersInfos.p1.score > gameInfos.playersInfos.p2.score)
            gameInfos.playersInfos.winner = {name: gameInfos.playersInfos.p1.name, score: gameInfos.playersInfos.p1.score}
        else
            gameInfos.playersInfos.winner = {name: gameInfos.playersInfos.p2.name, score: gameInfos.playersInfos.p2.score}
        bodyContent.playersInfos = gameInfos.playersInfos;
    } else if(gameInfos.mode === 0) {
        mode = `solo`;
        bodyContent.playerInfos = gameInfos.playersInfos;
        bodyContent.timer = gameInfos.timer;
        dispatch(checkLeaderboard(gameInfos.timer, gameInfos.playersInfos));
    }
    // POST du résultat vers l'API
    fetch(`${apiUrl}${mode}`, {
        method: 'POST',
        body: JSON.stringify( bodyContent ),
        headers: { 'Content-Type': 'application/json' }
    }).then( () => {
        
    })
    return {type: END_GAME, playersInfos: gameInfos.playersInfos}
}
// Effectue la fin d'un round
function endRound(dispatch, gameCards, gameInfos) {
    // Appelle des updates si c'est la fin du tour
    const compareResult = compareCards(gameCards.selectedCards);
    // Si c'est le mode deux joueurs, mise à jour des scores en fonction du résultat
    if(gameInfos.mode === 1)
        dispatch(updatePlayersInfos(compareResult, gameInfos.playersInfos));
    dispatch(updateCardsList(compareResult, resetSelectedCards(gameCards.cards), gameCards.selectedCards));
    if(shouldEndGame(gameInfos.cardsCount, gameCards.cards))
        return dispatch(endGame(dispatch, gameInfos));
    else return null;
}
// Modifie l'état d'une carte et ajoute sa valeur dans selectedCards
function toggleCard(cards, cardIndex, value) {
    cards.map((card, index) => {
        if(index === cardIndex) {
            card.isSelected = true;
        }
        return card;
    });
    return {type: TOGGLE_CARD, cardIndex, value, cards}
}
//

/*
Gestion du leaderboard
 */
// Récupère l'ensemble des informations du leaderboard
function fetchLeaderboard() {
    return dispatch => {
        return fetch(`http://localhost:3004/leaderboard`)
            .then(response => response.json())
            .then(json => dispatch(receiveLeaderboard(json)))
    }
}
// Reçoit et transmet le leaderboard au reducer
function receiveLeaderboard(json) {
    const leaderboard = json;
    return {type: RECEIVE_LEADERBOARD, leaderboard}
}
// Ajout d'une partie dans le leaderboard
function updateLeaderboard(leaderboard) {
    // POST du résultat vers l'API
    fetch(`http://localhost:3004/leaderboard`, {
        method: 'POST',
        body: JSON.stringify( leaderboard ),
        headers: { 'Content-Type': 'application/json' }
    }).then( () => {

    })
    return {type: UPDATE_LEADERBOARD, leaderboard}
}
// Compare le gagnant du partie avec le reste du tableau
function checkLeaderboard(timer, playerInfos) {
    return (dispatch, getState) => {
        const currentScore = {
            playerInfos,
            timer            
        }
        let leaderboard = getState().gameLeaderboard.leaderboard;
        if(leaderboard.solo.length > 0) {
            let position = -1;
            let isLower = false;
            leaderboard.solo.map((score, index) => {
                if(currentScore.timer < score.timer && !isLower) {
                    position = index;
                    isLower = true;
                }
                return score;
            });
            if(position === -1 && leaderboard.solo.length < 11) {
                leaderboard.solo.push(currentScore);
                return dispatch(updateLeaderboard(leaderboard));
            }
            else if(position > -1) {
                leaderboard.solo.splice(position, 0, currentScore);
                leaderboard.solo = leaderboard.solo.splice(0, 10);
                return dispatch(updateLeaderboard(leaderboard));
            }
        } else {
            leaderboard.solo.push(currentScore);
            return dispatch(updateLeaderboard(leaderboard));
        }
    }
}
//

/*
Fonctions exportées
 */
// Gestion de la récupération du mode de jeu (Home => "1P" ou "2P")
export function setMode(mode) {
    return {type: SET_MODE, mode}
}
// Gestion de l'état du jeu
export function setGameState(gameState) {
    return {type: SET_GAME_STATE, gameState}
}
// Gère le handleChange sur les input des noms des joueurs (Home => Inputs "1P" ou "2P")
export function setPlayerName(playersInfos) {
    return {type: SET_PLAYER_NAME, playersInfos}
}
// Initialisation du leaderboard au moment du chargement de l'App
export function initLeaderboard() {
    return dispatch => {
        return dispatch(fetchLeaderboard());
    }
}
// Lancement de la partie (Home => "Jouer")
export function initGame(cardsCount) {
    return dispatch => {
        dispatch(startTimer());
        return dispatch(fetchCards(cardsCount));
    }
}
// Réinitialise l'état de la partie
export function resetGame() {
    return {type: RESET_GAME}
}
// Trigger lorsqu'une carte est sélectionnée (Card => Clique sur la carte)
export function selectCard(index, value) {
    return (dispatch, getState) => {
        // Toggle de la carte sélectionnée
        let gameCards = getState().gameCards;
        dispatch(toggleCard(gameCards.cards, index, value));
        // Check si c'est la fin du tour
        const gameInfos = getState().gameInfos;
        gameCards = getState().gameCards;
        if(shouldCompareCards(gameCards.selectedCards)) {
            setTimeout(() => endRound(dispatch, gameCards, gameInfos), 1000);
        }
    }
}
//

//